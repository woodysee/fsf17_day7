/**
 * Client_side client.controller.js
 * Angular js
 */
"use strict";
(
    () => {
        "use strict";
        angular.module("RegApp").controller("RegVM", RegVM);

        RegVM.$inject = ["$http"];

        function RegVM($http) {
            const vm = this;
            vm.SELECT_DEFAULT = {
                name: "Please select",
                value: "0"
            };
            vm.selected = {
                email: "",
                nationality: ""
            };
            vm.nationalities = [
                {
                    name: "Singaporean",
                    value: "1"
                },
                {
                    name: "Indonesian",
                    value: "2"
                },
                {
                    name: "Thai",
                    value: "3"
                },
                {
                    name: "Malaysian",
                    value: "4"
                }
            ];
            vm.onSubmit = () => {
                console.log(vm.selected);
                console.log(vm.selected.email);
                console.log(vm.selected.password);
                console.log(vm.selected.confirmPassword);
                console.log(vm.selected.fullName);
                console.log(vm.selected.gender);
                console.log(vm.selected.dob);
                console.log(vm.selected.postalAddress);
                console.log(vm.selected.nationality);
                console.log(vm.selected.contactNo);
                $http.post(
                    "/api/submit",
                    vm.selected
                ).then(
                    (res) => {
                        console.log(res);
                    }
                ).catch(
                    (error) => {
                        console.log(error);
                    }
                )
            };

            vm.onlyFemales = () => {
                console.log("Only females are selected.");
                return vm.selected.gender == "Female";
            };

            //On load invokes the Registration form
            (
                () => {
                    vm.selected.nationality = "0";
                }
            )();

        };
    }
)();