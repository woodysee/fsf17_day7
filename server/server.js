/**
*App.js server side
*/

// Dependencies
const express = require("express");
const server = express();
const bodyParser = require("body-parser");

const NODE_PORT = process.env.PORT || 4000;

server.use(express.static(__dirname + "/../client/"));
server.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
server.use(bodyParser.json({ limit: '50mb' }));

let forms = []; //initialise empty forms

// Methods

server.get(
    "/api",
    (req, res) => {
        console.log("200: GET /api/submit");
        console.log(req.body);
        res.status(200).json(forms);
    }
);

server.post(
    "/api/submit",
    (req, res) => {
        console.log("200: POST /api/submit");
        console.log(req.body);
        forms.push(req.body);
    }
);

// Listener
server.listen(
    NODE_PORT,
    () => {
        console.log(`Listening with server/server.js at port ${NODE_PORT}`);
    }
);